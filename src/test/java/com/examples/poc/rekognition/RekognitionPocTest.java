package com.examples.poc.rekognition;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.rekognition.AmazonRekognition;
import com.amazonaws.services.rekognition.AmazonRekognitionClientBuilder;
import com.examples.poc.rekognition.utils.ImageUtil;
import org.junit.Test;

import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class RekognitionPocTest {

    private final static String BASE_PHOTO_PATH = "src/test/resources/images/rodrigo_front_serious_glasses.jpg";

    private final AmazonRekognition rekognitionClient = AmazonRekognitionClientBuilder
            .standard()
            .withRegion(Regions.DEFAULT_REGION)
            .build();

    private final ImageComparator imageComparator = new ImageComparator(rekognitionClient);



    @Test
    public void it_should_return_true_when_same_person() {
        Float similarity = 90F;
        ByteBuffer source = ImageUtil.getByteBuffer(BASE_PHOTO_PATH).get();

        List<String> targetPathList = Arrays.asList(
                "src/test/resources/images/rodrigo_front_serious_bare.jpg",
                "src/test/resources/images/rodrigo_front_serious_glasses.jpg",
                "src/test/resources/images/rodrigo_front_smile_glasses.jpg",
                "src/test/resources/images/rodrigo_front_smile2_glasses.jpg",
                "src/test/resources/images/rodrigo_front_smile_bare.jpg",
                "src/test/resources/images/rodrigo_side_serious_bare.jpg");

        targetPathList.forEach(t -> {
            ByteBuffer target = ImageUtil.getByteBuffer(t).get();

            assertTrue(imageComparator.compare(similarity, source, target));
        });
    }

    @Test
    public void it_should_return_false_when_different_person() {
        Float similarity = 90F;
        ByteBuffer source = ImageUtil.getByteBuffer(BASE_PHOTO_PATH).get();

        List<String> targetPathList = Arrays.asList(
                "src/test/resources/images/pablo_front_serious_bare.jpg",
                "src/test/resources/images/pablo_front_smile_bare.jpg");

        targetPathList.forEach(t -> {
            ByteBuffer target = ImageUtil.getByteBuffer(t).get();

            assertFalse(imageComparator.compare(similarity, source, target));
        });
    }

    @Test
    public void it_should_return_true_when_same_person_out_of_two() {
        Float similarity = 90F;
        ByteBuffer source = ImageUtil.getByteBuffer(BASE_PHOTO_PATH).get();

        List<String> targetPathList = Arrays.asList("src/test/resources/images/rodrigo_justin.jpg");

        targetPathList.forEach(t -> {
            ByteBuffer target = ImageUtil.getByteBuffer(t).get();

            assertTrue(imageComparator.compare(similarity, source, target));
        });
    }
}
