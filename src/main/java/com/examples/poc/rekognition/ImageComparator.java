package com.examples.poc.rekognition;

import com.amazonaws.services.rekognition.AmazonRekognition;
import com.amazonaws.services.rekognition.model.*;

import java.nio.ByteBuffer;
import java.util.List;

public class ImageComparator {

    private static final Float DEFAULT_THRESHOLD = 70f;
    private final AmazonRekognition rekognitionClient;

    public ImageComparator(AmazonRekognition rekognitionClient) {
        this.rekognitionClient = rekognitionClient;

    }

    public boolean compare (ByteBuffer sourceImageBytes, ByteBuffer targetImageBytes) {
        return compare(DEFAULT_THRESHOLD, sourceImageBytes, targetImageBytes);

    }

    public boolean compare(Float similarityThreshold, ByteBuffer sourceImageBytes, ByteBuffer targetImageBytes) {
        Image sourceImage = new Image()
                .withBytes(sourceImageBytes);
        Image targetImage = new Image()
                .withBytes(targetImageBytes);

        CompareFacesRequest request = new CompareFacesRequest()
                .withSourceImage(sourceImage)
                .withTargetImage(targetImage)
                .withSimilarityThreshold(similarityThreshold);

        CompareFacesResult compareFacesResult = rekognitionClient.compareFaces(request);

        List<CompareFacesMatch> faceDetails = compareFacesResult.getFaceMatches();
        for (CompareFacesMatch match : faceDetails) {
            ComparedFace face = match.getFace();
            BoundingBox position = face.getBoundingBox();
            System.out.println("Face at " + position.getLeft().toString()
                    + " " + position.getTop()
                    + " matches with " + face.getConfidence().toString()
                    + "% confidence.");

        }
        List<ComparedFace> uncompared = compareFacesResult.getUnmatchedFaces();

        System.out.println("There was " + uncompared.size()
                + " face(s) that did not match");
        System.out.println("Source image rotation: " + compareFacesResult.getSourceImageOrientationCorrection());
        System.out.println("targetImage image rotation: " + compareFacesResult.getTargetImageOrientationCorrection());

    return !faceDetails.isEmpty();
    }
}
