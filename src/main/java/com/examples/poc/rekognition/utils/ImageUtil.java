package com.examples.poc.rekognition.utils;

import com.amazonaws.util.IOUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.Optional;

public class ImageUtil {

    public static Optional<ByteBuffer> getByteBuffer(String path) {
        try (
                InputStream inputStream = new FileInputStream(new File(path))) {
            return Optional.of(ByteBuffer.wrap(IOUtils.toByteArray(inputStream)));
        }
        catch(Exception e)
        {
            System.out.println("Failed to load source image " + path);
            return Optional.empty();
        }
    }
}
