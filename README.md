## Face Recognition POC
The aim of this project is to test Amazon face detection
API.

### Configuration
Add the following environment variables:
+ AWS_ACCESS_KEY_ID
+ AWS_SECRET_ACCESS_KEY


For intellij testing:

`Run -> Edit Configurations -> Templates -> Junit`

### Links
+ [Amazon Example](https://docs.aws.amazon.com/rekognition/latest/dg/faces-comparefaces.html)



### Backlog
+ Persist known faces
+ Detect Face from bucket
